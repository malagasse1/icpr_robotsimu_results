#include "ros/ros.h"
#include <laser_assembler/AssembleScans.h>
#include "sensor_msgs/PointCloud.h"

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "periodic_snapshotter");
    ros::NodeHandle n;
    ros::service::waitForService("assemble_scans");
    ros::ServiceClient client = n.serviceClient<laser_assembler::AssembleScans>("assemble_scans");
    laser_assembler::AssembleScans srv;
    
    ros::Publisher chatter_pub = n.advertise<sensor_msgs::PointCloud>("my_cloud", 1000);

    ros::Rate loop_rate(5);

    while (ros::ok())
    {
        sensor_msgs::PointCloud msg;
        srv.request.begin = ros::Time(0,0);
        srv.request.end   = ros::Time::now();

        if (client.call(srv)) {
            msg = srv.response.cloud;
            chatter_pub.publish(msg);
        }
        else
            printf("Service call failed\n");

        ros::spinOnce();

        loop_rate.sleep();
    }


    return 0;
}

