#include "ros/ros.h"
#include "std_msgs/Float64.h"

#include <sstream>

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  ros::init(argc, argv, "lidar_rotation");
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise<std_msgs::Float64>("/laser_velocity_controller/command", 1000);

  ros::Rate loop_rate(2);

  double v;
  if( !n.getParam("velocity", v))
  {
        v= 2.0;
  }

  while (ros::ok())
  {
    
    std_msgs::Float64 msg;

    msg.data = v;

    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}

